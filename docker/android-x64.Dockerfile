ARG BASE_TAG=latest
FROM cross-compiler-base:${BASE_TAG}

ENV CROSS_TRIPLE x86_64-linux-android
ENV CROSS_ROOT /usr/${CROSS_TRIPLE}
ENV PATH ${PATH}:${CROSS_ROOT}/bin
ENV LD_LIBRARY_PATH ${CROSS_ROOT}/lib:${LD_LIBRARY_PATH}
ENV PKG_CONFIG_PATH ${CROSS_ROOT}/lib/pkgconfig:${PKG_CONFIG_PATH}

RUN apt-get update && apt-get install -y python

ENV NDK android-ndk-r20
ENV ANDROID_NDK_API 21
ENV ANDROID_ARCH x86_64

COPY scripts/build_android_toolchain.sh /scripts/
RUN ./scripts/build_android_toolchain.sh

RUN cd ${CROSS_ROOT}/bin && \
    ln -s ${CROSS_TRIPLE}-clang ${CROSS_TRIPLE}-cc
