ARG BASE_TAG=latest
FROM cross-compiler-base:${BASE_TAG}

RUN apt-get update --yes && apt-get install --no-install-recommends --yes \
    flex ncurses-dev gperf gawk texinfo help2man python-dev \
    && apt-get clean --yes

COPY \
    scripts/install-crosstool-ng-toolchain.sh \
    crosstool-ng/linux-armv7.config \
    /cross/

ENV XCC_PREFIX /usr/xcc
# Build and install the toolchain, cleaning up artifacts afterwards.
RUN cd /tmp \
    && /cross/install-crosstool-ng-toolchain.sh -p "${XCC_PREFIX}" -c /cross/*.config \
    && rm -rf /tmp/*

ENV CROSS_TRIPLE armv7-unknown-linux-gnueabi
ENV CROSS_ROOT ${XCC_PREFIX}/${CROSS_TRIPLE}
ENV PATH ${PATH}:${CROSS_ROOT}/bin
ENV LD_LIBRARY_PATH ${CROSS_ROOT}/lib:${LD_LIBRARY_PATH}
ENV PKG_CONFIG_PATH ${CROSS_ROOT}/lib/pkgconfig:${PKG_CONFIG_PATH}
