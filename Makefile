IMAGE_PREFIX = cross-compiler
TAG = 1.0
DOCKER = docker

PLATFORMS = \
	android-arm \
	android-arm64 \
	android-x64 \
	android-x86 \
	darwin-x64 \
	linux-arm \
	linux-armv7 \
	linux-arm64 \
	linux-x64 \
	linux-x86 \
	windows-x64 \
	windows-x86

.PHONY: $(PLATFORMS)

all:
	for i in $(PLATFORMS); do \
		$(MAKE) $$i; \
	done

base:
	$(DOCKER) build -t $(IMAGE_PREFIX)-base:$(TAG) .

$(PLATFORMS): base
	$(DOCKER) build \
		-t $(IMAGE_PREFIX)-$@:$(TAG) \
		-t $(IMAGE_PREFIX)-$@:latest \
		--build-arg BASE_TAG=$(TAG) -f docker/$@.Dockerfile docker

push:
	docker push $(IMAGE_PREFIX)-$(PLATFORM)

push-all:
	for i in $(PLATFORMS); do \
		PLATFORM=$$i $(MAKE) push; \
	done

pull:
	docker pull $(IMAGE_PREFIX)-$(PLATFORM):latest

pull-all:
	for i in $(PLATFORMS); do \
		PLATFORM=$$i $(MAKE) pull; \
	done
